package cmd

import (
	"fmt"

	// "edu/cmd"

	"github.com/spf13/cobra/cobra/cmd"
	uuid "github.com/uuid6/uuid6go-proto"
)

func main() {
	cmd.Execute()

	// var gen uuid.UUIDv7Generator
	// gen.SubsecondPrecisionLength = 12
	// id := gen.Next()

	fmt.Println("kek")
	// fmt.Println(id.ToString())
	// fmt.Println(id.ToBinaryString())
	// fmt.Println(id.ToMicrosoftString())
	// fmt.Println(id.Time())
	// fmt.Println(id.Timestamp())
}

type Person struct {
	uuid      uuid.UUIDv7
	firstName string
	lastName  string
}

type Position struct {
	uuid         uuid.UUIDv7
	positionName string
}

type Employee struct {
	uuid     uuid.UUIDv7
	person   Person
	position Position
}

func (e *Employee) PrintAll() {
	fmt.Println(e.person.uuid, e.person.lastName, e.person.firstName, e.position.uuid, e.position.positionName, e.uuid)
}
